using System.Runtime.InteropServices;
using System;

namespace My_First_Api_Project
{
    public class Aero
    {
        public DateTime Flight_Time { get; set; }

        public int AirTemperatureC { get; set; }

        public int PlaneLength { get; set; } 

        public int PassengerCount { get; set; }

        public string NamePlanes { get; set; }

        public string CityName { get; set; } 

        public string FlightNumber { get; set; }
    }

}

