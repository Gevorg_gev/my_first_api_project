using System.Runtime.InteropServices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace My_First_Api_Project.Controllers
{
    [ApiController]
    [Route("[controller]")]

    public class AeroController : ControllerBase
    {
        private static readonly string[] Name_Of_Planes = new[]
        {
            "Ural Aiirlines" , "Air Arabia" , "Armenian Airlines" , "Air Russia" , "Aeroflot" , "S7 Airlines"
        };

        private static readonly string[] Name_Of_Cities = new[]
        {
            "Yerevan" , "Washington" , "Los Angeles" , "Moscow" , "Belgrade" , "Chicago" , "New York"
        };

        private static readonly string[] Number_Of_Flights = new[]
        {
            "8697" , "2365" , "8845" , "1548" , "7845" , "4587" , "3654" 
        };

        private readonly ILogger<AeroController> control;

        public AeroController(ILogger<AeroController> control)
        {
            this.control = control;
        }
         
        [HttpGet]

        public IEnumerable<Aero> Get() 
        {
            var rng = new Random();
            return Enumerable.Range(1, 8).Select(index => new Aero
            {
                Flight_Time = DateTime.Now.AddDays(index),

                AirTemperatureC = rng.Next(-20, 40),

                PlaneLength = rng.Next(25, 400),

                PassengerCount = rng.Next(1, 250),

                NamePlanes = Name_Of_Planes[rng.Next(Name_Of_Planes.Length)],

                CityName = Name_Of_Cities[rng.Next(Name_Of_Cities.Length)],

                FlightNumber = Number_Of_Flights[rng.Next(Number_Of_Flights.Length)] 
            })
            .ToArray();
        }
    }
}